Diceware-style Wordlist Generator
---------------------------------

Here's a bit of code you can use to craft a printable wordlist for
generating passwords with dice.  This isn't strictly the "official"
Diceware wordlist, but that's OK!  It's something you can print out
and keep with your dice for when you need a human-memorable password.
This doesn't require any official standard, since you're not sharing
those wordlists with anyone else anyway.

To generate a wordlist, just run:

    ./generator.py

It will create `diceware.pdf` in the current directory, which is a
printable one-page wordlist built from whatever vocabulary you prefer
(it defaults to using the shortest simple words it can find in
`/usr/share/dict/words` on your system).

You'll need the "reportlab" python module and something that supplies
`/usr/share/dict/words` on your machine.

For example, on a debian system, I used:

    apt install python3-reportlab wamerican make

If there are words in your wordlist that you don't like, or that you
think might be unpleasant to have to type in as passwords, you can
make a file called `stopwords.txt` and place them there, one word per
line.  Subsequent runs of `./generator.py` will never include those
words:

    echo poop >> stopwords.txt
    ./generator.py

You might want to
[read more about the idea of diceware](https://en.wikipedia.org/wiki/Diceware).

Remember, the more words you use in a password, the more secure that
password is!
