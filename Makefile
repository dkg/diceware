#!/usr/bin/make -f

diceware.pdf: /usr/share/dict/words generator.py
	./generator.py

clean:
	rm -f diceware.pdf

.PHONY: clean
