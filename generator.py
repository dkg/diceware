#!/usr/bin/env python3

import sys
from reportlab.pdfgen import canvas #type: ignore
from reportlab.lib.pagesizes import letter,landscape #type: ignore
from reportlab.lib.units import inch #type: ignore
import re
import random
from typing import List, Pattern

with open('/usr/share/dict/words') as n:
    x:List[str] = n.read().split('\n')

stopwords:List[str]
try:
    with open('stopwords.txt') as n:
        stopwords = n.read().split('\n')
except:
    stopwords = []

# this hard-codes an expectation that our wordlist uses latin characters:
m:Pattern[str] = re.compile('[^a-z]')

# this hard-codes that we want short words:
x = list(filter(lambda a: (not m.search(a)) and len(a) <=4 and len(a) > 2 and a not in stopwords, x))

# this hard-codes what kind of (and how many) dice we're using
symbols:List[str] = ['1', '2', '3', '4', '5', '6']
fields:int = 4

count:int = len(symbols) ** fields

if (count > len(x)):
    raise Exception("only {len} words, needed {count}".format(count=count, len=len(x)))

# pick a random subset of the available words, and then put them back
# in alphabetical order:
random.shuffle(x)
l:List[str] = sorted(x[:count])

c = canvas.Canvas('diceware.pdf', pagesize=landscape(letter))
wid, hi = landscape(letter)
margin = inch/2.0
inwid = wid - 2*margin
inhi = hi - 2*margin
fontsz = inhi/((len(symbols)+1)*len(symbols)*2)
c.setFont("Helvetica", fontsz)
lbuf = fontsz/3

k:List[int] = []
z:int = 0
while z < fields:
    k.append(0)
    z = z + 1

def convert(k:List[int]) -> str:
    ret:str = ''
    x:int
    for x in k:
        ret += symbols[x]
    return ret

def increment(k:List[int]) -> List[int]:
    ix:int = fields -1
    while (ix >= 0):
        if k[ix] < len(symbols) - 1:
            k[ix] = k[ix] + 1
            return k
        else:
            k[ix] = 0
            ix -= 1
    return k

for i in l:
    c.drawString(margin + ((k[0]* inwid/len(symbols)) + (k[2]//2) * inwid/(len(symbols)*(len(symbols)/2))),
                 hi - margin - (k[1] * inhi/(len(symbols))) - ((((k[2]%2)*len(symbols)) + k[3]) * inhi/(len(symbols)* (len(symbols)*2+1))) - fontsz/2,
                 convert(k) + " " + i)
    k = increment(k)

ix:int = 0
while (ix <= len(symbols)):
    bar = fontsz + hi - margin - ix * (hi-2*margin)/len(symbols)
    c.line(margin - lbuf, bar, wid - margin - lbuf, bar);
    col = margin + ix * (wid-2*margin)/len(symbols) - lbuf
    c.line(col, margin + fontsz, col, hi - margin + fontsz);
    ix += 1
    
c.showPage()
c.save()
